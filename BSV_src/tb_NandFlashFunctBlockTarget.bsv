/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
--
-- Copyright (c) 2014  Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

////////////////////////////////////////////////////////////////////////////////
// Name of the Module	: Tb to test ONFI 4.0 + target
// Coded by				: Laxmeesha.S
// The whole test case is done assuming no ECC data is stored in the page. Hence Program and read page operations end exactly at the col addr corresponding to page size 
//  excluding the spare sub-page size . (i.e only 4096B and not 4096B+128B)
////////////////////////////////////////////////////////////////////////////////


//package tb_NandFlashFunctBlockTarget ;

import Vector::*;
import BRAM :: * ;
import InterfaceNandFlashFunctBlockTarget :: * ;
import NandFlashFunctBlockTarget :: * ;

typedef enum {St1 ,St2 ,St3 ,St4 ,St5 ,St6 ,St7 ,St8 ,St9 ,St10, St11, St12, St13, St14, St15
} State_op deriving( Bits, Eq ) ;

(*synthesize*)
module mkTbNandFlashTarget ();
	ONFI_Target_Interface nandFlashTarget <- mkNandFlashTarget () ;
	Reg#(Bit#(16)) state_count <- mkReg(0) ; // This is to keep track of clk events.
	Reg#(State_op) op_state    <- mkReg(St1) ;
	Reg#(Bit#(16)) col_addr    <- mkReg(0) ;
	Reg#(Bit#(16)) error_count <- mkReg(0) ;
	Reg#(Bit#(8)) data_A      <- mkReg('hAA);
	Reg#(Bit#(8)) data_5      <- mkReg('h55);
	Reg#(Bit#(8)) cmd_80      <- mkReg('h80);
	Reg#(Bit#(8)) cmd_70      <- mkReg('h70);
	Reg#(Bit#(8)) cmd_78      <- mkReg('h78);
	Reg#(Bit#(8)) cmd_30      <- mkReg('h30);
	Reg#(Bit#(8)) cmd_32      <- mkReg('h32);
	Reg#(Bit#(8)) cmd_00      <- mkReg('h00);
	Reg#(Bit#(8)) cmd_01      <- mkReg('h01);
	Reg#(Bit#(8)) cmd_03      <- mkReg('h03);
	Reg#(Bit#(8)) cmd_06      <- mkReg('h06);
	Reg#(Bit#(8)) cmd_E0      <- mkReg('hE0);
	Reg#(Bit#(8)) cmd_FF      <- mkReg('hFF);
	Reg#(Bit#(8)) cmd_10      <- mkReg('h10);
	Reg#(Bit#(8)) cmd_11      <- mkReg('h11);
	Reg#(Bit#(16)) cmd_400     <- mkReg('h400);
	Reg#(Bit#(16)) cmd_401     <- mkReg('h401);
	Reg#(Bit#(16)) cmd_403     <- mkReg('h403);
	Reg#(Bit#(16)) max_val     <- mkReg('hFFF);
	Reg#(Bit#(1)) busy_active_low  <- mkReg(1);
	Reg#(Bit#(1)) enable_active_low  <- mkReg(0);
	Reg#(Bit#(1)) disable_active_low <- mkReg(1);
	Reg#(Bit#(1)) enable_active_high <- mkReg(1);
	Reg#(Bit#(1)) disable_active_high <- mkReg(0);
	
	//Drive all control signals to disabled state.
	rule rl_disable_control_signals (state_count == 0) ;
		nandFlashTarget.onfi_target_interface._onfi_ce_n_m(disable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
		nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
		state_count <= state_count + 1 ; 
	endrule
	
	//Reset the target first.
	rule rl_reset_first (state_count == 1) ;
		//Send 'hFF command
		nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
		nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
		nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_FF);
		state_count <= state_count + 1 ;
	endrule
	
	rule rl_wait_after_reset (state_count == 2) ;
		case (op_state)
			//Wait 2 cycles and then poll busy flag.
			St1 : begin
				op_state <= St2 ;
				//disable all control signals
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			      end
			St2 : begin
				op_state <= St3 ;
			      end
			St3 : begin
				if(nandFlashTarget.onfi_target_interface.t_ready_busy_n_() == busy_active_low)
					op_state <= St4 ;
			      end
			St4 : begin
				op_state <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Test1 : Write addr0 in LUN0 and then addr0 in LUN1 (concurrent LUN program operation).Wait and then do read from the addr(concurrent LUN read) and compare the data  ////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	

	//Write into some addr in LUN0. Say addr = 0 (LUN 0 . plane 0, block 0, page 0).
	rule rl_writing_into_lun0 (state_count == 3) ;
		case (op_state)
			St1 : begin
				//Send 80h first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_80);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr. Will always be zero since it is only page writable.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr. Will always be zero since it is only page writable.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Start sending data by toggling we_n pin.
				//Disable we.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St8 ;
			      end
			St8 : begin
				//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_A);
				op_state <= St9 ;
				col_addr <= col_addr + 1;
			      end
			St9 : begin
				//Disable we again
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
	                       	//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_5);
				if(col_addr < max_val)
				begin
					op_state <= St7 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St11 ;
					col_addr <= 0 ;
				end
			      end
			St11 : begin
				//End command series by giving 10h.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_10);
				op_state <= St12 ;
			      end
			St12 : begin
				//Disable all control signals 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			        op_state <= St1;// Observe rdy flags in waveform.
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	//Write into some addr in LUN1. Say addr = 0 (LUN 1 . plane 0, block 0, page 0).
	rule rl_writing_into_lun1 (state_count == 4) ;
		case (op_state)
			St1 : begin
				//Send 80h first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_80);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr. Will always be zero since it is only page writable.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr. Will always be zero since it is only page writable.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h40);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00); //11th bit is LUN select
				op_state <= St6 ;
			      end
			St6 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00); //11th bit is LUN select
				op_state <= St7 ;
			      end
			St7 : begin
				//Start sending data by toggling we_n pin.
				//Disable we.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St8 ;
			      end
			St8 : begin
				//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_A);
				op_state <= St9 ;
				col_addr <= col_addr + 1;
			      end
			St9 : begin
				//Disable we again
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
	                       	//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_5);
				if(col_addr < max_val)
				begin
					op_state <= St7 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St11 ;
					col_addr <= 0 ;
				end
			      end
			St11 : begin
				//End command series by giving 10h.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_10);
				op_state <= St12 ;
			      end
			St12 : begin
				//Disable all control signals 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			        op_state <= St1;// Observe rdy flags in waveform.
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_status_after_write (state_count == 5) ;
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 70h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_70);
			      end
			St2 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St3 ;
			      end
			St3 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[6] == busy_active_low) //7th bit is the ready/busy bit
					op_state <= St4 ;
			      end
			St4 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_lun0_data (state_count == 6) ;
		case (op_state)
			St1 : begin
				//Send 00h command first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Send 30h command to start reading from target. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_30);
				op_state <= St8 ;
			      end
			St8 : begin
				op_state <= St1 ;
				state_count <= state_count + 1;
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			      end
		endcase
	endrule
	
	rule rl_read_lun1_data (state_count == 7) ;
		case (op_state)
			St1 : begin
				//Send 00h command first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h40);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Send 30h command to start reading from target. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_30);
				op_state <= St8 ;
			      end
			St8 : begin
				op_state <= St1 ;
				state_count <= state_count + 1;
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			      end
		endcase
	endrule
	
	rule rl_read_status_after_read (state_count == 8) ;
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 70h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_70);
			      end
			St2 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St3 ;
			      end
			St3 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[6] == busy_active_low) //7th bit is the ready/busy bit
					op_state <= St4 ;
			      end
			St4 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_lun1_cache_reg2 (state_count == 9) ;
		case (op_state)
			St1 : begin
				//Send 00h command. This will read from LUN1 since status was being read from LUN1
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St2 ;
			      end
			St2 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St3 ;
				if(col_addr > 0) // that is read due to every even re_n active toggle.
				begin
					if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_5)
						error_count <= error_count + 1;
				end
				col_addr <= col_addr + 1;
			      end
			St3 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St4 ;
			      end
			St4 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St5 ;
				//Here read due to every odd re_n active toggle.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_A)
					error_count <= error_count + 1;
			      end
			St5 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(col_addr < max_val )
				begin
					op_state <= St2 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St6 ;
					col_addr  <= 0;
				end
			      end
			St6 : begin
				//Last even read will be pending.Do it here.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St7 ;
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_5)
					error_count <= error_count + 1;
			     end
			St7 : begin
				//check errors.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(error_count > 0)
					$display("Read Error present in LUN1");
				error_count <= 0;
				op_state <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_lun0_cache_reg0 (state_count == 10) ;
		case (op_state)
			St1 : begin
				//Need to select the cahce register 0 first to start reading . Send h06 command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_06);
				op_state <= St2 ;
			      end
			St2 : begin
				//send col address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//send col address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//send address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St5 ;
			      end
			St5 : begin
				//send address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//send address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//send 'hE0 command to start reading
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_E0);
				op_state <= St8 ;
			      end
			St8 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St9 ;
				if(col_addr > 0) // that is read due to every even re_n active toggle.
				begin
					if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_5)
						error_count <= error_count + 1;
				end
				col_addr <= col_addr + 1;
			      end
			St9 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St11 ;
				//Here read due to every odd re_n active toggle.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_A)
					error_count <= error_count + 1;
			      end
			St11 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(col_addr < max_val )
				begin
					op_state <= St8 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St12 ;
					col_addr  <= 0;
				end
			      end
			St12 : begin
				//Last even read will be pending.Do it here.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St13 ;
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_5)
					error_count <= error_count + 1;
			     end
			St13 : begin
				//check errors.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(error_count > 0)
					$display("Read Error present in LUN0");
				error_count <= 0;
				op_state <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
					
	rule rl_finish_simulation (state_count == 11) ;
		$finish ;
	endrule 

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////                                           END OF TEST1                                               /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Test1.1 : Write addr0 in LUN0 and then addr0 in LUN1 (concurrent LUN program operation).Wait and then do read from the addr(concurrent LUN read) and compare the data ///////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	//Write into some addr in LUN0. Say addr = 0 (LUN 0 . plane 0, block 0, page 0).
	rule rl_writing_into_lun0 (state_count == 3) ;
		case (op_state)
			St1 : begin
				//Send 80h first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_80);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr. Will always be zero since it is only page writable.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr. Will always be zero since it is only page writable.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h08);
				col_addr <= 'd2048;
				op_state <= St4 ;
			      end
			St4 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Start sending data by toggling we_n pin.
				//Disable we.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St8 ;
			      end
			St8 : begin
				//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_A);
				op_state <= St9 ;
				col_addr <= col_addr + 1;
			      end
			St9 : begin
				//Disable we again
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
	                       	//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_5);
				if(col_addr < max_val)
				begin
					op_state <= St7 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St11 ;
					col_addr <= 0 ;
				end
			      end
			St11 : begin
				//End command series by giving 10h.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_10);
				op_state <= St12 ;
			      end
			St12 : begin
				//Disable all control signals 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			        op_state <= St1;// Observe rdy flags in waveform.
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	//Write into some addr in LUN1. Say addr = 0 (LUN 1 . plane 0, block 0, page 0).
	rule rl_writing_into_lun1 (state_count == 4) ;
		case (op_state)
			St1 : begin
				//Send 80h first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_80);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr. Will always be zero since it is only page writable.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr. Will always be zero since it is only page writable.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h40);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00); //11th bit is LUN select
				op_state <= St6 ;
			      end
			St6 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00); //11th bit is LUN select
				op_state <= St7 ;
			      end
			St7 : begin
				//Start sending data by toggling we_n pin.
				//Disable we.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St8 ;
			      end
			St8 : begin
				//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_A);
				op_state <= St9 ;
				col_addr <= col_addr + 1;
			      end
			St9 : begin
				//Disable we again
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
	                       	//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_5);
				if(col_addr < max_val)
				begin
					op_state <= St7 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St11 ;
					col_addr <= 0 ;
				end
			      end
			St11 : begin
				//End command series by giving 10h.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_10);
				op_state <= St12 ;
			      end
			St12 : begin
				//Disable all control signals 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			        op_state <= St1;// Observe rdy flags in waveform.
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_status_after_write (state_count == 5) ;
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 70h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_70);
			      end
			St2 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St3 ;
			      end
			St3 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[6] == busy_active_low) //7th bit is the ready/busy bit
					op_state <= St4 ;
			      end
			St4 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_lun0_data (state_count == 6) ;
		case (op_state)
			St1 : begin
				//Send 00h command first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Send 30h command to start reading from target. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_30);
				op_state <= St8 ;
			      end
			St8 : begin
				op_state <= St1 ;
				state_count <= state_count + 1;
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			      end
		endcase
	endrule
	
	rule rl_read_lun1_data (state_count == 7) ;
		case (op_state)
			St1 : begin
				//Send 00h command first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h40);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Send 30h command to start reading from target. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_30);
				op_state <= St8 ;
			      end
			St8 : begin
				op_state <= St1 ;
				state_count <= state_count + 1;
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			      end
		endcase
	endrule
	
	rule rl_read_status_after_read (state_count == 8) ;
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 70h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_70);
			      end
			St2 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St3 ;
			      end
			St3 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[6] == busy_active_low) //7th bit is the ready/busy bit
					op_state <= St4 ;
			      end
			St4 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_lun1_cache_reg2 (state_count == 9) ;
		case (op_state)
			St1 : begin
				//Send 00h command. This will read from LUN1 since status was being read from LUN1
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St2 ;
			      end
			St2 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St3 ;
				if(col_addr > 0) // that is read due to every even re_n active toggle.
				begin
					if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_5)
						error_count <= error_count + 1;
				end
				col_addr <= col_addr + 1;
			      end
			St3 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St4 ;
			      end
			St4 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St5 ;
				//Here read due to every odd re_n active toggle.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_A)
					error_count <= error_count + 1;
			      end
			St5 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(col_addr < max_val )
				begin
					op_state <= St2 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St6 ;
					col_addr  <= 0;
				end
			      end
			St6 : begin
				//Last even read will be pending.Do it here.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St7 ;
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_5)
					error_count <= error_count + 1;
			     end
			St7 : begin
				//check errors.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(error_count > 0)
					$display("Read Error present in LUN1");
				error_count <= 0;
				op_state <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_lun0_cache_reg0 (state_count == 10) ;
		case (op_state)
			St1 : begin
				//Need to select the cahce register 0 first to start reading . Send h06 command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_06);
				op_state <= St2 ;
			      end
			St2 : begin
				//send col address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//send col address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//send address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St5 ;
			      end
			St5 : begin
				//send address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//send address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//send 'hE0 command to start reading
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_E0);
				op_state <= St8 ;
			      end
			St8 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St9 ;
				if(col_addr > 0) // that is read due to every even re_n active toggle.
				begin
					if(col_addr <= 'd2048)
					begin
						if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != 'hFF)
							error_count <= error_count + 1;
					end
					else
					begin
						if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_5)
							error_count <= error_count + 1;
					end
				end
				col_addr <= col_addr + 1;
			      end
			St9 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St11 ;
				//Here read due to every odd re_n active toggle.
				if(col_addr <= 'd2048)
				begin
					if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != 'hFF)
						error_count <= error_count + 1;
				end
				else
				begin
					if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_A)
						error_count <= error_count + 1;
				end
			      end
			St11 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(col_addr < max_val )
				begin
					op_state <= St8 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St12 ;
					col_addr  <= 0;
				end
			      end
			St12 : begin
				//Last even read will be pending.Do it here.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St13 ;
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_5)
					error_count <= error_count + 1;
			     end
			St13 : begin
				//check errors.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(error_count > 0)
					$display("Read Error present in LUN0");
				error_count <= 0;
				op_state <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
					
	rule rl_finish_simulation (state_count == 11) ;
		$finish ;
	endrule 
*/
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////                                         END OF TEST1.1                                               /////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Test2 :Write addr0 in lun0 plane0 and addr0 in LUN0 plane1(concurrent plane operation). Follow similar process for LUN1 (that covers concurrent plane and concurrent lun program operation). Then do multi-plane read on programmed address on both luns(That covers concurrent plane and concurrent lun read operation)                        ////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*	
	//Write into addr0(plane0) and adddr0(plane1) in LUN0. 
	rule rl_writing_into_lun0_plane0 (state_count == 3) ;
		case (op_state)
			St1 : begin
				//Send 80h first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_80);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Start sending data by toggling we_n pin.
				//Disable we.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St8 ;
			      end
			St8 : begin
				//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_A);
				op_state <= St9 ;
				col_addr <= col_addr + 1;
			      end
			St9 : begin
				//Disable we again
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
	                       	//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_5);
				if(col_addr < max_val)
				begin
					op_state <= St7 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St11 ;
					col_addr <= 0 ;
				end
			      end
			St11 : begin
				//End command series by giving 11h. To queue program
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_11);
				op_state <= St12 ;
			      end
			St12 : begin
				//Disable all control signals 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			        op_state <= St1;// Observe rdy flags in waveform.
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_writing_into_lun0_plane1 (state_count == 4) ;
		case (op_state)
			St1 : begin
				//Send 80h first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_80);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h18);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Start sending data by toggling we_n pin.
				//Disable we.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St8 ;
			      end
			St8 : begin
				//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_5);
				op_state <= St9 ;
				col_addr <= col_addr + 1;
			      end
			St9 : begin
				//Disable we again
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
	                       	//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_A);
				if(col_addr < max_val)
				begin
					op_state <= St7 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St11 ;
					col_addr <= 0 ;
				end
			      end
			St11 : begin
				//End command series by giving 10h. Earlier queued page and this page both should be written to target
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_10);
				op_state <= St12 ;
			      end
			St12 : begin
				//Disable all control signals 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			        op_state <= St1;// Observe rdy flags in waveform.
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	//Write into addr0(plane0) and adddr0(plane1) in LUN1. 
	rule rl_writing_into_lun1_plane0 (state_count == 5) ;
		case (op_state)
			St1 : begin
				//Send 80h first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_80);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h40);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Start sending data by toggling we_n pin.
				//Disable we.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St8 ;
			      end
			St8 : begin
				//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_A);
				op_state <= St9 ;
				col_addr <= col_addr + 1;
			      end
			St9 : begin
				//Disable we again
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
	                       	//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_5);
				if(col_addr < max_val)
				begin
					op_state <= St7 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St11 ;
					col_addr <= 0 ;
				end
			      end
			St11 : begin
				//End command series by giving 11h. To queue program
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_11);
				op_state <= St12 ;
			      end
			St12 : begin
				//Disable all control signals 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			        op_state <= St1;// Observe rdy flags in waveform.
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_writing_into_lun1_plane1 (state_count == 6) ;
		case (op_state)
			St1 : begin
				//Send 80h first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_80);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h58);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Start sending data by toggling we_n pin.
				//Disable we.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St8 ;
			      end
			St8 : begin
				//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_5);
				op_state <= St9 ;
				col_addr <= col_addr + 1;
			      end
			St9 : begin
				//Disable we again
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
	                       	//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_A);
				if(col_addr < max_val)
				begin
					op_state <= St7 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St11 ;
					col_addr <= 0 ;
				end
			      end
			St11 : begin
				//End command series by giving 10h. Earlier queued page and this page both should be written to target
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_10);
				op_state <= St12 ;
			      end
			St12 : begin
				//Disable all control signals 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			        op_state <= St1;// Observe rdy flags in waveform.
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_status_after_write (state_count == 7) ;
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 70h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_70);
			      end
			St2 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St3 ;
			      end
			St3 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[6] == busy_active_low) //7th bit is the ready/busy bit
					op_state <= St4 ;
			      end
			St4 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_lun0_data_plane0 (state_count == 8) ;
		case (op_state)
			St1 : begin
				//Send 00h command first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Send 32h command to queue plane0 for read. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_32);
				op_state <= St8 ;
			      end
			St8 : begin
				op_state <= St1 ;
				state_count <= state_count + 1;
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			      end
		endcase
	endrule
	
	rule rl_read_lun0_data_plane1 (state_count == 9) ;
		case (op_state)
			St1 : begin
				//Send 00h command first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h18);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Send 30h command to start reading queued and present page. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_30);
				op_state <= St8 ;
			      end
			St8 : begin
				op_state <= St1 ;
				state_count <= state_count + 1;
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			      end
		endcase
	endrule
	
	rule rl_read_lun1_data_plane0 (state_count == 10) ;
		case (op_state)
			St1 : begin
				//Send 00h command first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send  col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send  col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h40);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Send 32h command to queue page . 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_32);
				op_state <= St8 ;
			      end
			St8 : begin
				op_state <= St1 ;
				state_count <= state_count + 1;
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			      end
		endcase
	endrule
	
	rule rl_read_lun1_data_plane1 (state_count == 11) ;
		case (op_state)
			St1 : begin
				//Send 00h command first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h58);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Send 30h command to start reading queue page and present page. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_30);
				op_state <= St8 ;
			      end
			St8 : begin
				op_state <= St1 ;
				state_count <= state_count + 1;
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			      end
		endcase
	endrule
	
	//Will try to monitor the status now with 78h command (and check status of LUN0) instead of 70h(which would have targetted LUN1).
	rule rl_read_status_after_read_lun0 (state_count == 12) ;
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 78h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_78);
			      end
			St2 : begin
				op_state <= St3 ;
				//Send  address 00 for lun0 plane0
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
			      end
			St3 : begin
				op_state <= St4 ;
				//Send  address 00 for lun0 plane0
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
			      end
			St4 : begin
				op_state <= St5 ;
				//Send  address 00 for lun0 plane0
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
			      end
			St5 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St6 ;
			      end
			St6 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[6] == busy_active_low) //7th bit is the ready/busy bit
					op_state <= St7 ;
			      end
			St7 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_lun0_cache_reg0 (state_count == 13) ;
		case (op_state)
			St1 : begin
				//Send 00h command. This will read from LUN0 since status was being read from LUN0
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St7 ;
				if(col_addr > 0) // that is read due to every even re_n active toggle.
				begin
					if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_5)
						error_count <= error_count + 1;
				end
				col_addr <= col_addr + 1;
			      end
			St7 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St8 ;
			      end
			St8 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St9 ;
				//Here read due to every odd re_n active toggle.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_A)
					error_count <= error_count + 1;
			      end
			St9 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(col_addr < max_val )
				begin
					op_state <= St6 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St10 ;
					col_addr  <= 0;
				end
			      end
			St10 : begin
				//Last even read will be pending.Do it here.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St11 ;
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_5)
					error_count <= error_count + 1;
			     end
			St11 : begin
				//check errors.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(error_count > 0)
					$display("Read Error present in LUN0 plane0");
				error_count <= 0;
				op_state <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	//Since LUN0 is ready , just select cache register 1 and start reading.
	rule rl_read_lun0_cache_reg1 (state_count == 14) ;
		case (op_state)
			St1 : begin
				//Need to select the cahce register 1 first to start reading . Send h06 command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_06);
				op_state <= St2 ;
			      end
			St2 : begin
				//send col address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//send col address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//send  address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h18);
				op_state <= St5 ;
			      end
			St5 : begin
				//send  address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//send  address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//send 'hE0 command to start reading
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_E0);
				op_state <= St8 ;
			      end
			St8 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St9 ;
				if(col_addr > 0) // that is read due to every even re_n active toggle.
				begin
					if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_A)
						error_count <= error_count + 1;
				end
				col_addr <= col_addr + 1;
			      end
			St9 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St11 ;
				//Here read due to every odd re_n active toggle.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_5)
					error_count <= error_count + 1;
			      end
			St11 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(col_addr < max_val )
				begin
					op_state <= St8 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St12 ;
					col_addr  <= 0;
				end
			      end
			St12 : begin
				//Last even read will be pending.Do it here.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St13 ;
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_A)
					error_count <= error_count + 1;
			     end
			St13 : begin
				//check errors.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(error_count > 0)
					$display("Read Error present in LUN0 plane1");
				error_count <= 0;
				op_state <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	//Will try to monitor the status now with 78h command (and check status of LUN1) instead of 70h(which would have targetted LUN0).
	rule rl_read_status_after_read_lun1 (state_count == 15) ;
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 78h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_78);
			      end
			St2 : begin
				op_state <= St3 ;
				//Send  address 00 for lun1 plane0
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h40);
			      end
			St3 : begin
				op_state <= St4 ;
				//Send  address 00 for lun1 plane0
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
			      end
			St4 : begin
				op_state <= St5 ;
				//Send  address 00 for lun1 plane0
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
			      end
			St5 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St6 ;
			      end
			St6 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[6] == busy_active_low) //7th bit is the ready/busy bit
					op_state <= St7 ;
			      end
			St7 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_lun1_cache_reg2 (state_count == 16) ;
		case (op_state)
			St1 : begin
				//Need to select the cahce register 2 first to start reading . Send h06 command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_06);
				op_state <= St2 ;
			      end
			St2 : begin
				//send col address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//send col address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//send  address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h40);
				op_state <= St5 ;
			      end
			St5 : begin
				//send  address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//send  address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//send 'hE0 command to start reading
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_E0);
				op_state <= St8 ;
			      end
			St8 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St9 ;
				if(col_addr > 0) // that is read due to every even re_n active toggle.
				begin
					if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_5)
						error_count <= error_count + 1;
				end
				col_addr <= col_addr + 1;
			      end
			St9 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St11 ;
				//Here read due to every odd re_n active toggle.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_A)
					error_count <= error_count + 1;
			      end
			St11 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(col_addr < max_val )
				begin
					op_state <= St8 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St12 ;
					col_addr  <= 0;
				end
			      end
			St12 : begin
				//Last even read will be pending.Do it here.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St13 ;
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_5)
					error_count <= error_count + 1;
			     end
			St13 : begin
				//check errors.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(error_count > 0)
					$display("Read Error present in LUN1 plane0");
				error_count <= 0;
				op_state <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_lun1_cache_reg3 (state_count == 17) ;
		case (op_state)
			St1 : begin
				//Need to select the cahce register 3 first to start reading . Send h06 command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_06);
				op_state <= St2 ;
			      end
			St2 : begin
				//send col address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//send col address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//send  address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h58);
				op_state <= St5 ;
			      end
			St5 : begin
				//send  address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//send  address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//send 'hE0 command to start reading
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_E0);
				op_state <= St8 ;
			      end
			St8 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St9 ;
				if(col_addr > 0) // that is read due to every even re_n active toggle.
				begin
					if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_A)
						error_count <= error_count + 1;
				end
				col_addr <= col_addr + 1;
			      end
			St9 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St11 ;
				//Here read due to every odd re_n active toggle.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_5)
					error_count <= error_count + 1;
			      end
			St11 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(col_addr < max_val )
				begin
					op_state <= St8 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St12 ;
					col_addr  <= 0;
				end
			      end
			St12 : begin
				//Last even read will be pending.Do it here.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St13 ;
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_A)
					error_count <= error_count + 1;
			     end
			St13 : begin
				//check errors.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(error_count > 0)
					$display("Read Error present in LUN1 plane1");
				error_count <= 0;
				op_state <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_finish_simulation (state_count == 18) ;
		$finish ;
	endrule 
*/
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////                                           END OF TEST2                                               /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Test3 : Write into 2 pages in plane0 of lun0 by means of program page cache. Repeat for lun1. Then                        ////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*	
	//Write into addr0(plane0) and adddr0(plane1) in LUN0. 
	rule rl_writing_into_lun0_plane0_page0 (state_count == 3) ;
		case (op_state)
			St1 : begin
				//Send 80h first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_80);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Start sending data by toggling we_n pin.
				//Disable we.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St8 ;
			      end
			St8 : begin
				//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_A);
				op_state <= St9 ;
				col_addr <= col_addr + 1;
			      end
			St9 : begin
				//Disable we again
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
	                       	//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_5);
				if(col_addr < max_val)
				begin
					op_state <= St7 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St11 ;
					col_addr <= 0 ;
				end
			      end
			St11 : begin
				//End command series by giving 11h. To queue program
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h15);
				op_state <= St12 ;
			      end
			St12 : begin
				//Disable all control signals 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			        op_state <= St1;// Observe rdy flags in waveform.
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_status_after_write_page0_lun0 (state_count == 4) ; //Rdy will be high when the data is written into data register. After that can feed next page into cache.
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 70h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_70);
			      end
			St2 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St3 ;
			      end
			St3 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[6] == busy_active_low) //7th bit is the ready/busy bit
					op_state <= St4 ;
			      end
			St4 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_writing_into_lun0_plane0_page1 (state_count == 5) ;
		case (op_state)
			St1 : begin
				//Send 80h first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_80);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_01);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Start sending data by toggling we_n pin.
				//Disable we.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St8 ;
			      end
			St8 : begin
				//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_5);
				op_state <= St9 ;
				col_addr <= col_addr + 1;
			      end
			St9 : begin
				//Disable we again
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
	                       	//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_A);
				if(col_addr < max_val)
				begin
					op_state <= St7 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St11 ;
					col_addr <= 0 ;
				end
			      end
			St11 : begin
				//End command series by giving 11h. To queue program
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h15);
				op_state <= St12 ;
			      end
			St12 : begin
				//Disable all control signals 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			        op_state <= St1;// Observe rdy flags in waveform.
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	//Write into addr0(plane0) and adddr0(plane1) in LUN1. 
	rule rl_writing_into_lun1_plane0_page0 (state_count == 6) ;
		case (op_state)
			St1 : begin
				//Send 80h first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_80);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h40);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send Block addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send Block addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Start sending data by toggling we_n pin.
				//Disable we.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St8 ;
			      end
			St8 : begin
				//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_A);
				op_state <= St9 ;
				col_addr <= col_addr + 1;
			      end
			St9 : begin
				//Disable we again
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
	                       	//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_5);
				if(col_addr < max_val)
				begin
					op_state <= St7 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St11 ;
					col_addr <= 0 ;
				end
			      end
			St11 : begin
				//End command series by giving 11h. To queue program
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h15);
				op_state <= St12 ;
			      end
			St12 : begin
				//Disable all control signals 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			        op_state <= St1;// Observe rdy flags in waveform.
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_status_after_write_page0_lun1 (state_count == 7) ;  //Rdy will be high when the data is written into data register. After that can feed next page into cache.
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 70h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_70);
			      end
			St2 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St3 ;
			      end
			St3 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[6] == busy_active_low) //7th bit is the ready/busy bit
					op_state <= St4 ;
			      end
			St4 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_writing_into_lun1_plane0_page1 (state_count == 8) ;
		case (op_state)
			St1 : begin
				//Send 80h first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_80);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h41);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Start sending data by toggling we_n pin.
				//Disable we.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St8 ;
			      end
			St8 : begin
				//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_5);
				op_state <= St9 ;
				col_addr <= col_addr + 1;
			      end
			St9 : begin
				//Disable we again
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
	                       	//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_A);
				if(col_addr < max_val)
				begin
					op_state <= St7 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St11 ;
					col_addr <= 0 ;
				end
			      end
			St11 : begin
				//End command series by giving 11h. To queue program
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h15);
				op_state <= St12 ;
			      end
			St12 : begin
				//Disable all control signals 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			        op_state <= St1;// Observe rdy flags in waveform.
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_status_after_write_page1 (state_count == 9) ;  //ARdy will be high when the data is written into target from data register
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 70h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_70);
			      end
			St2 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St3 ;
			      end
			St3 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[5] == busy_active_low) //6th bit is the Aready/busy bit
					op_state <= St4 ;
			      end
			St4 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	//Now need to read from all pages and compare the read data.First lun0 data (read page cache not simple read).
	//First read page0 in lun0 to cache.
	rule rl_read_lun0_data_plane0_page0 (state_count == 10) ;
		case (op_state)
			St1 : begin
				//Send 00h command first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Send 30h command to start reading queued and present page. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_30);
				op_state <= St8 ;
			      end
			St8 : begin
				op_state <= St1 ;
				state_count <= state_count + 1;
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			      end
		endcase
	endrule
	
	rule rl_read_status_after_read_lun0_page0 (state_count == 11) ;  
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 70h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_70);
			      end
			St2 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St3 ;
			      end
			St3 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[6] == busy_active_low) //7th bit is the ready/busy bit
					op_state <= St4 ;
			      end
			St4 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	//Now do read page cache sequential and start reading next data into data reg and meanwhile start eading out data from cache.
	rule rl_read_lun0_page1_to_data_reg (state_count == 12) ;  
		nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
		nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
		nandFlashTarget.onfi_target_interface._data_from_nfc_m('h31);
		state_count <= state_count + 1;
	endrule
	
	//Now read page0 in lun1 to cache.
	rule rl_read_lun1_data_plane0_page0 (state_count == 13) ;
		case (op_state)
			St1 : begin
				//Send 00h command first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h40);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Send 30h command to start reading queued and present page. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_30);
				op_state <= St8 ;
			      end
			St8 : begin
				op_state <= St1 ;
				state_count <= state_count + 1;
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			      end
		endcase
	endrule
	
	rule rl_read_status_after_read_lun1_page0 (state_count == 14) ;  
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 70h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_70);
			      end
			St2 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St3 ;
			      end
			St3 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[6] == busy_active_low) //7th bit is the ready/busy bit
					op_state <= St4 ;
			      end
			St4 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	//Now do read page cache sequential and start reading next data into data reg and meanwhile start eading out data from cache.
	rule rl_read_lun1_page0_to_data_reg (state_count == 15) ;  
		nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
		nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
		nandFlashTarget.onfi_target_interface._data_from_nfc_m('h31);
		state_count <= state_count + 1;
	endrule
	
	rule rl_read_lun0_cache_reg0_page0 (state_count == 16) ;
		case (op_state)
			St1 : begin
				//Need to select the cahce register 0 first to start reading . Send h06 command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_06);
				op_state <= St2 ;
			      end
			St2 : begin
				//send col address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//send col address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//send  address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St5 ;
			      end
			St5 : begin
				//send  address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//send  address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//send 'hE0 command to start reading
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_E0);
				op_state <= St8 ;
			      end
			St8 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St9 ;
				if(col_addr > 0) // that is read due to every even re_n active toggle.
				begin
					if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_5)
						error_count <= error_count + 1;
				end
				col_addr <= col_addr + 1;
			      end
			St9 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St11 ;
				//Here read due to every odd re_n active toggle.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_A)
					error_count <= error_count + 1;
			      end
			St11 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(col_addr < max_val )
				begin
					op_state <= St8 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St12 ;
					col_addr  <= 0;
				end
			      end
			St12 : begin
				//Last even read will be pending.Do it here.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St13 ;
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_5)
					error_count <= error_count + 1;
			     end
			St13 : begin
				//check errors.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(error_count > 0)
					$display("Read Error present in LUN0 plane0 page0");
				error_count <= 0;
				op_state <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_lun1_cache_reg0_page0 (state_count == 17) ;
		case (op_state)
			St1 : begin
				//Need to select the cahce register 2 first to start reading . Send h06 command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_06);
				op_state <= St2 ;
			      end
			St2 : begin
				//send col address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//send col address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//send  address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h40);
				op_state <= St5 ;
			      end
			St5 : begin
				//send  address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//send  address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//send 'hE0 command to start reading
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_E0);
				op_state <= St8 ;
			      end
			St8 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St9 ;
				if(col_addr > 0) // that is read due to every even re_n active toggle.
				begin
					if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_5)
						error_count <= error_count + 1;
				end
				col_addr <= col_addr + 1;
			      end
			St9 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St11 ;
				//Here read due to every odd re_n active toggle.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_A)
					error_count <= error_count + 1;
			      end
			St11 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(col_addr < max_val )
				begin
					op_state <= St8 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St12 ;
					col_addr  <= 0;
				end
			      end
			St12 : begin
				//Last even read will be pending.Do it here.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St13 ;
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_5)
					error_count <= error_count + 1;
			     end
			St13 : begin
				//check errors.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(error_count > 0)
					$display("Read Error present in LUN1 plane0 page0");
				error_count <= 0;
				op_state <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	//Select LUN0 for status read using 78h
	rule rl_read_status_for_target_to_data_reg_page1_lun0 (state_count == 18) ;
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 78h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_78);
			      end
			St2 : begin
				op_state <= St3 ;
				//Send  address 00 for lun0 plane0
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h01);
			      end
			St3 : begin
				op_state <= St4 ;
				//Send  address 00 for lun0 plane0
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
			      end
			St4 : begin
				op_state <= St5 ;
				//Send  address 00 for lun0 plane0
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
			      end
			St5 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St6 ;
			      end
			St6 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[5] == busy_active_low) //6th bit is the Aready/busy bit
					op_state <= St7 ;
			      end
			St7 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	//Start reading page1 from data reg to cache reg.
	rule rl_read_page1_lun0_data_to_cache_reg (state_count == 19) ;  
		nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
		nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
		nandFlashTarget.onfi_target_interface._data_from_nfc_m('h3F);
		state_count <= state_count + 1;
	endrule
	
	//Select LUN1 for status read using 78h
	rule rl_read_status_for_target_to_data_reg_page1_lun1 (state_count == 20) ;
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 78h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_78);
			      end
			St2 : begin
				op_state <= St3 ;
				//Send  address 00 for lun1 plane0
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h41);
			      end
			St3 : begin
				op_state <= St4 ;
				//Send  address 00 for lun1 plane0
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
			      end
			St4 : begin
				op_state <= St5 ;
				//Send  address 00 for lun1 plane0
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
			      end
			St5 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St6 ;
			      end
			St6 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[5] == busy_active_low) //6th bit is the Aready/busy bit
					op_state <= St7 ;
			      end
			St7 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	//Start reading page1 from data reg to cache reg.
	rule rl_read_page1_lun1_data_to_cache_reg (state_count == 21) ;  
		nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
		nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
		nandFlashTarget.onfi_target_interface._data_from_nfc_m('h3F);
		state_count <= state_count + 1;
	endrule
	
	//Select LUN0 for status read using 78h
	rule rl_read_status_for_data_to_cache_reg_page1_lun0 (state_count == 22) ;
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 78h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_78);
			      end
			St2 : begin
				op_state <= St3 ;
				//Send  address 00 for lun0 plane0
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h01);
			      end
			St3 : begin
				op_state <= St4 ;
				//Send  address 00 for lun0 plane0
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
			      end
			St4 : begin
				op_state <= St5 ;
				//Send  address 00 for lun0 plane0
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
			      end
			St5 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St6 ;
			      end
			St6 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[6] == busy_active_low) //7th bit is the ready/busy bit
					op_state <= St7 ;
			      end
			St7 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_lun0_cache_reg0_page1 (state_count == 23) ;
		case (op_state)
			St1 : begin
				//Send 00h command. This will read from LUN0 since status was being read from LUN0
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St7 ;
				if(col_addr > 0) // that is read due to every even re_n active toggle.
				begin
					if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_A)
						error_count <= error_count + 1;
				end
				col_addr <= col_addr + 1;
			      end
			St7 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St8 ;
			      end
			St8 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St9 ;
				//Here read due to every odd re_n active toggle.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_5)
					error_count <= error_count + 1;
			      end
			St9 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(col_addr < max_val )
				begin
					op_state <= St6 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St10 ;
					col_addr  <= 0;
				end
			      end
			St10 : begin
				//Last even read will be pending.Do it here.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St11 ;
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_A)
					error_count <= error_count + 1;
			     end
			St11 : begin
				//check errors.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(error_count > 0)
					$display("Read Error present in LUN0 plane0 page1");
				error_count <= 0;
				op_state <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	//Select LUN1 for status read using 78h
	rule rl_read_status_for_data_to_cache_reg_page1_lun1 (state_count == 24) ;
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 78h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_78);
			      end
			St2 : begin
				op_state <= St3 ;
				//Send  address 00 for lun0 plane0
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h41);
			      end
			St3 : begin
				op_state <= St4 ;
				//Send  address 00 for lun0 plane0
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
			      end
			St4 : begin
				op_state <= St5 ;
				//Send  address 00 for lun0 plane0
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
			      end
			St5 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St6 ;
			      end
			St6 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[6] == busy_active_low) //7th bit is the ready/busy bit
					op_state <= St7 ;
			      end
			St7 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_lun1_cache_reg0_page1 (state_count == 25) ;
		case (op_state)
			St1 : begin
				//Send 00h command. This will read from LUN0 since status was being read from LUN0
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St7 ;
				if(col_addr > 0) // that is read due to every even re_n active toggle.
				begin
					if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_A)
						error_count <= error_count + 1;
				end
				col_addr <= col_addr + 1;
			      end
			St7 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St8 ;
			      end
			St8 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St9 ;
				//Here read due to every odd re_n active toggle.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_5)
					error_count <= error_count + 1;
			      end
			St9 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(col_addr < max_val )
				begin
					op_state <= St6 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St10 ;
					col_addr  <= 0;
				end
			      end
			St10 : begin
				//Last even read will be pending.Do it here.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St11 ;
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_A)
					error_count <= error_count + 1;
			     end
			St11 : begin
				//check errors.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(error_count > 0)
					$display("Read Error present in LUN1 plane0 page1");
				error_count <= 0;
				op_state <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_finish_simulation (state_count == 26) ;
		$finish ;
	endrule
*/
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////                                           END OF TEST3                                               /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Test4 : Write into 4 pages in plane0 of lun0 by means of program page cache.Then Do read page cache                                                                  ////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*	
	//Write into addr0(plane0) and adddr0(plane1) in LUN0. 
	rule rl_writing_into_lun0_plane0_page0 (state_count == 3) ;
		case (op_state)
			St1 : begin
				//Send 80h first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_80);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Start sending data by toggling we_n pin.
				//Disable we.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St8 ;
			      end
			St8 : begin
				//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_A);
				op_state <= St9 ;
				col_addr <= col_addr + 1;
			      end
			St9 : begin
				//Disable we again
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
	                       	//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_5);
				if(col_addr < max_val)
				begin
					op_state <= St7 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St11 ;
					col_addr <= 0 ;
				end
			      end
			St11 : begin
				//End command series by giving 11h. To queue program
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h15);
				op_state <= St12 ;
			      end
			St12 : begin
				//Disable all control signals 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			        op_state <= St1;// Observe rdy flags in waveform.
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_status_after_write_page0_lun0 (state_count == 4) ; //Rdy will be high when the data is written into data register. After that can feed next page into cache.
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 70h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_70);
			      end
			St2 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St3 ;
			      end
			St3 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[6] == busy_active_low) //7th bit is the ready/busy bit
					op_state <= St4 ;
			      end
			St4 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_writing_into_lun0_plane0_page1 (state_count == 5) ;
		case (op_state)
			St1 : begin
				//Send 80h first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_80);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_01);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Start sending data by toggling we_n pin.
				//Disable we.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St8 ;
			      end
			St8 : begin
				//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_5);
				op_state <= St9 ;
				col_addr <= col_addr + 1;
			      end
			St9 : begin
				//Disable we again
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
	                       	//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_A);
				if(col_addr < max_val)
				begin
					op_state <= St7 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St11 ;
					col_addr <= 0 ;
				end
			      end
			St11 : begin
				//End command series by giving 11h. To queue program
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h15);
				op_state <= St12 ;
			      end
			St12 : begin
				//Disable all control signals 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			        op_state <= St1;// Observe rdy flags in waveform.
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_status_after_write_page1_lun0 (state_count == 6) ; //Rdy will be high when the data is written into data register. After that can feed next page into cache.
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 70h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_70);
			      end
			St2 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St3 ;
			      end
			St3 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[6] == busy_active_low) //7th bit is the ready/busy bit
					op_state <= St4 ;
			      end
			St4 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_writing_into_lun0_plane0_page2 (state_count == 7) ; // In this case page0 of block2 of lun0 (i.e next block in plane0)
		case (op_state)
			St1 : begin
				//Send 80h first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_80);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h08);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Start sending data by toggling we_n pin.
				//Disable we.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St8 ;
			      end
			St8 : begin
				//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_5);
				op_state <= St9 ;
				col_addr <= col_addr + 1;
			      end
			St9 : begin
				//Disable we again
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
	                       	//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_A);
				if(col_addr < max_val)
				begin
					op_state <= St7 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St11 ;
					col_addr <= 0 ;
				end
			      end
			St11 : begin
				//End command series by giving 11h. To queue program
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h15);
				op_state <= St12 ;
			      end
			St12 : begin
				//Disable all control signals 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			        op_state <= St1;// Observe rdy flags in waveform.
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_status_after_write_page2_lun0 (state_count == 8) ; //Rdy will be high when the data is written into data register. After that can feed next page into cache.
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 70h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_70);
			      end
			St2 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St3 ;
			      end
			St3 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[6] == busy_active_low) //7th bit is the ready/busy bit
					op_state <= St4 ;
			      end
			St4 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_writing_into_lun0_plane0_page3 (state_count == 9) ; // In this case page1 of block2 of lun0 (i.e next block in plane0)
		case (op_state)
			St1 : begin
				//Send 80h first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_80);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h09);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h02);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h02);
				op_state <= St7 ;
			      end
			St7 : begin
				//Start sending data by toggling we_n pin.
				//Disable we.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St8 ;
			      end
			St8 : begin
				//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_A);
				op_state <= St9 ;
				col_addr <= col_addr + 1;
			      end
			St9 : begin
				//Disable we again
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
	                       	//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_5);
				if(col_addr < max_val)
				begin
					op_state <= St7 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St11 ;
					col_addr <= 0 ;
				end
			      end
			St11 : begin
				//End command series by giving 11h. To queue program
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h15);
				op_state <= St12 ;
			      end
			St12 : begin
				//Disable all control signals 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			        op_state <= St1;// Observe rdy flags in waveform.
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_status_after_write_page3_lun0 (state_count == 10) ; //Check when ardy goes high since it is the last write.
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 70h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_70);
			      end
			St2 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St3 ;
			      end
			St3 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[5] == busy_active_low) //7th bit is the ready/busy bit
					op_state <= St4 ;
			      end
			St4 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_lun0_data_plane0_page0 (state_count == 11) ;
		case (op_state)
			St1 : begin
				//Send 00h command first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Send 30h command to start reading queued and present page. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_30);
				op_state <= St8 ;
			      end
			St8 : begin
				op_state <= St1 ;
				state_count <= state_count + 1;
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			      end
		endcase
	endrule
	
	rule rl_read_status_after_read_lun0_page0 (state_count == 12) ;  
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 70h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_70);
			      end
			St2 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St3 ;
			      end
			St3 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[6] == busy_active_low) //7th bit is the ready/busy bit
					op_state <= St4 ;
			      end
			St4 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	//Now do read page cache sequential and start reading next data into data reg and meanwhile start eading out data from cache.
	rule rl_read_lun0_page1_to_data_reg (state_count == 13) ;  
		nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
		nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
		nandFlashTarget.onfi_target_interface._data_from_nfc_m('h31);
		state_count <= state_count + 1;
	endrule
	
	rule rl_read_lun0_cache_reg0_for_page0 (state_count == 14) ;
		case (op_state)
			St1 : begin
				//Send 00h command. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St2 ;
			      end
			St2 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St3 ;
				if(col_addr > 0) // that is read due to every even re_n active toggle.
				begin
					if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_5)
						error_count <= error_count + 1;
				end
				col_addr <= col_addr + 1;
			      end
			St3 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St4 ;
			      end
			St4 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St5 ;
				//Here read due to every odd re_n active toggle.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_A)
					error_count <= error_count + 1;
			      end
			St5 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(col_addr < max_val )
				begin
					op_state <= St2 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St6 ;
					col_addr  <= 0;
				end
			      end
			St6 : begin
				//Last even read will be pending.Do it here.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St7 ;
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_5)
					error_count <= error_count + 1;
			     end
			St7 : begin
				//check errors.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(error_count > 0)
					$display("Read Error present in LUN1");
				error_count <= 0;
				op_state <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_status_after_read_lun0_data_reg_page1 (state_count == 15) ;  
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 70h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_70);
			      end
			St2 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St3 ;
			      end
			St3 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[5] == busy_active_low) //6th bit is the Aready bit
					op_state <= St4 ;
			      end
			St4 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_lun0_data_plane0_page2 (state_count == 16) ; //Start moving data reg to cache and then page 2 to data reg
		case (op_state)
			St1 : begin
				//Send 00h command first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h08);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Send 30h command to start reading queued and present page. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h31);
				op_state <= St8 ;
			      end
			St8 : begin
				op_state <= St1 ;
				state_count <= state_count + 1;
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			      end
		endcase
	endrule
	
	rule rl_read_status_after_read_lun0_page1 (state_count == 17) ;  
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 70h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_70);
			      end
			St2 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St3 ;
			      end
			St3 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[6] == busy_active_low) //7th bit is the Ready bit
					op_state <= St4 ;
			      end
			St4 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_lun0_cache_reg0_for_page1 (state_count == 18) ;
		case (op_state)
			St1 : begin
				//Send 00h command. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St2 ;
			      end
			St2 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St3 ;
				if(col_addr > 0) // that is read due to every even re_n active toggle.
				begin
					if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_A)
						error_count <= error_count + 1;
				end
				col_addr <= col_addr + 1;
			      end
			St3 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St4 ;
			      end
			St4 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St5 ;
				//Here read due to every odd re_n active toggle.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_5)
					error_count <= error_count + 1;
			      end
			St5 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(col_addr < max_val )
				begin
					op_state <= St2 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St6 ;
					col_addr  <= 0;
				end
			      end
			St6 : begin
				//Last even read will be pending.Do it here.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St7 ;
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_A)
					error_count <= error_count + 1;
			     end
			St7 : begin
				//check errors.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(error_count > 0)
					$display("Read Error present in LUN1");
				error_count <= 0;
				op_state <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_status_after_read_lun0_data_reg_page2 (state_count == 19) ;  
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 70h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_70);
			      end
			St2 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St3 ;
			      end
			St3 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[5] == busy_active_low) //6th bit is the Aready bit
					op_state <= St4 ;
			      end
			St4 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_lun0_page3_to_data_reg (state_count == 20) ;  
		nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
		nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
		nandFlashTarget.onfi_target_interface._data_from_nfc_m('h31);
		state_count <= state_count + 1;
	endrule
	
	rule rl_read_status_after_read_lun0_page2 (state_count == 21) ;  
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 70h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_70);
			      end
			St2 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St3 ;
			      end
			St3 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[6] == busy_active_low) //7th bit is the Ready bit
					op_state <= St4 ;
			      end
			St4 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_lun0_cache_reg0_for_page2 (state_count == 22) ;
		case (op_state)
			St1 : begin
				//Send 00h command. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St2 ;
			      end
			St2 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St3 ;
				if(col_addr > 0) // that is read due to every even re_n active toggle.
				begin
					if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_A)
						error_count <= error_count + 1;
				end
				col_addr <= col_addr + 1;
			      end
			St3 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St4 ;
			      end
			St4 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St5 ;
				//Here read due to every odd re_n active toggle.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_5)
					error_count <= error_count + 1;
			      end
			St5 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(col_addr < max_val )
				begin
					op_state <= St2 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St6 ;
					col_addr  <= 0;
				end
			      end
			St6 : begin
				//Last even read will be pending.Do it here.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St7 ;
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_A)
					error_count <= error_count + 1;
			     end
			St7 : begin
				//check errors.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(error_count > 0)
					$display("Read Error present in LUN1");
				error_count <= 0;
				op_state <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_status_after_read_lun0_data_reg_page3 (state_count == 23) ;  
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 70h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_70);
			      end
			St2 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St3 ;
			      end
			St3 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[5] == busy_active_low) //6th bit is the Aready bit
					op_state <= St4 ;
			      end
			St4 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_page3_lun0_data_to_cache_reg (state_count == 24) ;  
		nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
		nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
		nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
		nandFlashTarget.onfi_target_interface._data_from_nfc_m('h3F);
		state_count <= state_count + 1;
	endrule
	
	rule rl_read_status_after_read_lun0_page3 (state_count == 25) ;  
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 70h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_70);
			      end
			St2 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St3 ;
			      end
			St3 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[6] == busy_active_low) //7th bit is the Ready bit
					op_state <= St4 ;
			      end
			St4 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_lun0_cache_reg0_for_page3 (state_count == 26) ;
		case (op_state)
			St1 : begin
				//Send 00h command. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St2 ;
			      end
			St2 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St3 ;
				if(col_addr > 0) // that is read due to every even re_n active toggle.
				begin
					if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_5)
						error_count <= error_count + 1;
				end
				col_addr <= col_addr + 1;
			      end
			St3 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St4 ;
			      end
			St4 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St5 ;
				//Here read due to every odd re_n active toggle.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_A)
					error_count <= error_count + 1;
			      end
			St5 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(col_addr < max_val )
				begin
					op_state <= St2 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St6 ;
					col_addr  <= 0;
				end
			      end
			St6 : begin
				//Last even read will be pending.Do it here.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St7 ;
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_5)
					error_count <= error_count + 1;
			     end
			St7 : begin
				//check errors.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(error_count > 0)
					$display("Read Error present in LUN0");
				error_count <= 0;
				op_state <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_finish_simulation (state_count == 27) ;
		$finish ;
	endrule
*/
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////                                           END OF TEST4                                               /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Test5 : Write into 4 pages in plane0 of lun0 by means of multi-plane program page cache.Then Do read page multi plane                                               ////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*	
	//Write into addr0(plane0) and adddr0(plane1) in LUN0. 
	rule rl_writing_into_lun0_plane0_page0 (state_count == 3) ;
		case (op_state)
			St1 : begin
				//Send 80h first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_80);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Start sending data by toggling we_n pin.
				//Disable we.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St8 ;
			      end
			St8 : begin
				//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_A);
				op_state <= St9 ;
				col_addr <= col_addr + 1;
			      end
			St9 : begin
				//Disable we again
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
	                       	//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_5);
				if(col_addr < max_val)
				begin
					op_state <= St7 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St11 ;
					col_addr <= 0 ;
				end
			      end
			St11 : begin
				//End command series by giving 11h. To queue program
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_11);
				op_state <= St12 ;
			      end
			St12 : begin
				//Disable all control signals 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			        op_state <= St1;// Observe rdy flags in waveform.
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_writing_into_lun0_plane1_page0 (state_count == 4) ;
		case (op_state)
			St1 : begin
				//Send 80h first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_80);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h18);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Start sending data by toggling we_n pin.
				//Disable we.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St8 ;
			      end
			St8 : begin
				//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_A);
				op_state <= St9 ;
				col_addr <= col_addr + 1;
			      end
			St9 : begin
				//Disable we again
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
	                       	//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_5);
				if(col_addr < max_val)
				begin
					op_state <= St7 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St11 ;
					col_addr <= 0 ;
				end
			      end
			St11 : begin
				//End command series by giving 15h. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h15);
				op_state <= St12 ;
			      end
			St12 : begin
				//Disable all control signals 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			        op_state <= St1;// Observe rdy flags in waveform.
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_status_first (state_count == 5) ;  
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 70h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_70);
			      end
			St2 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St3 ;
			      end
			St3 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[6] == busy_active_low) //7th bit is the Ready bit
					op_state <= St4 ;
			      end
			St4 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_writing_into_lun0_plane0_page1 (state_count == 6) ;
		case (op_state)
			St1 : begin
				//Send 80h first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_80);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h01);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Start sending data by toggling we_n pin.
				//Disable we.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St8 ;
			      end
			St8 : begin
				//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_5);
				op_state <= St9 ;
				col_addr <= col_addr + 1;
			      end
			St9 : begin
				//Disable we again
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
	                       	//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_A);
				if(col_addr < max_val)
				begin
					op_state <= St7 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St11 ;
					col_addr <= 0 ;
				end
			      end
			St11 : begin
				//End command series by giving 11h. To queue program
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_11);
				op_state <= St12 ;
			      end
			St12 : begin
				//Disable all control signals 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			        op_state <= St1;// Observe rdy flags in waveform.
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_writing_into_lun0_plane1_page1 (state_count == 7) ;
		case (op_state)
			St1 : begin
				//Send 80h first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_80);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h19);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send Block addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send Block addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Start sending data by toggling we_n pin.
				//Disable we.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St8 ;
			      end
			St8 : begin
				//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_5);
				op_state <= St9 ;
				col_addr <= col_addr + 1;
			      end
			St9 : begin
				//Disable we again
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
	                       	//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_A);
				if(col_addr < max_val)
				begin
					op_state <= St7 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St11 ;
					col_addr <= 0 ;
				end
			      end
			St11 : begin
				//End command series by giving 15h. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h15);
				op_state <= St12 ;
			      end
			St12 : begin
				//Disable all control signals 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			        op_state <= St1;// Observe rdy flags in waveform.
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_status_second (state_count == 8) ;  
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 70h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_70);
			      end
			St2 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St3 ;
			      end
			St3 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[5] == busy_active_low) //6th bit is the AReady bit
					op_state <= St4 ;
			      end
			St4 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_lun0_data_plane0_page0 (state_count == 9) ;
		case (op_state)
			St1 : begin
				//Send 00h command first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Send 32h command to queue plane0 for read. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_32);
				op_state <= St8 ;
			      end
			St8 : begin
				op_state <= St1 ;
				state_count <= state_count + 1;
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			      end
		endcase
	endrule
	
	rule rl_read_lun0_data_plane1_page0 (state_count == 10) ;
		case (op_state)
			St1 : begin
				//Send 00h command first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h18);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Send 30h command to start reading queued and present page. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_30);
				op_state <= St8 ;
			      end
			St8 : begin
				op_state <= St1 ;
				state_count <= state_count + 1;
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			      end
		endcase
	endrule
	
	rule rl_read_status_third (state_count == 11) ;  
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 70h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_70);
			      end
			St2 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St3 ;
			      end
			St3 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[6] == busy_active_low) //7th bit is the Ready bit
					op_state <= St4 ;
			      end
			St4 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_lun0_cache_reg1_page1 (state_count == 12) ;
		case (op_state)
			St1 : begin
				//Send 00h command. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St2 ;
			      end
			St2 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St3 ;
				if(col_addr > 0) // that is read due to every even re_n active toggle.
				begin
					if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_5)
						error_count <= error_count + 1;
				end
				col_addr <= col_addr + 1;
			      end
			St3 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St4 ;
			      end
			St4 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St5 ;
				//Here read due to every odd re_n active toggle.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_A)
					error_count <= error_count + 1;
			      end
			St5 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(col_addr < max_val )
				begin
					op_state <= St2 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St6 ;
					col_addr  <= 0;
				end
			      end
			St6 : begin
				//Last even read will be pending.Do it here.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St7 ;
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_5)
					error_count <= error_count + 1;
			     end
			St7 : begin
				//check errors.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(error_count > 0)
					$display("Read Error present in LUN0");
				error_count <= 0;
				op_state <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_lun0_cache_reg0_page0 (state_count == 13) ;
		case (op_state)
			St1 : begin
				//Need to select the cahce register 0 first to start reading . Send h06 command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_06);
				op_state <= St2 ;
			      end
			St2 : begin
				//send col address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//send col address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//send  address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St5 ;
			      end
			St5 : begin
				//send  address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//send  address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//send 'hE0 command to start reading
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_E0);
				op_state <= St8 ;
			      end
			St8 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St9 ;
				if(col_addr > 0) // that is read due to every even re_n active toggle.
				begin
					if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_5)
						error_count <= error_count + 1;
				end
				col_addr <= col_addr + 1;
			      end
			St9 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St11 ;
				//Here read due to every odd re_n active toggle.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_A)
					error_count <= error_count + 1;
			      end
			St11 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(col_addr < max_val )
				begin
					op_state <= St8 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St12 ;
					col_addr  <= 0;
				end
			      end
			St12 : begin
				//Last even read will be pending.Do it here.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St13 ;
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_5)
					error_count <= error_count + 1;
			     end
			St13 : begin
				//check errors.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(error_count > 0)
					$display("Read Error present in LUN0");
				error_count <= 0;
				op_state <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_lun0_data_plane0_page1 (state_count == 14) ;
		case (op_state)
			St1 : begin
				//Send 00h command first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_01);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send block addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send block addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Send 32h command to queue plane0 for read. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_32);
				op_state <= St8 ;
			      end
			St8 : begin
				op_state <= St1 ;
				state_count <= state_count + 1;
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			      end
		endcase
	endrule
	
	rule rl_read_lun0_data_plane1_page1 (state_count == 15) ;
		case (op_state)
			St1 : begin
				//Send 00h command first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h19);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Send 30h command to start reading queued and present page. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_30);
				op_state <= St8 ;
			      end
			St8 : begin
				op_state <= St1 ;
				state_count <= state_count + 1;
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			      end
		endcase
	endrule
	
	rule rl_read_status_fourth (state_count == 16) ;  
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 70h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_70);
			      end
			St2 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St3 ;
			      end
			St3 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[6] == busy_active_low) //7th bit is the Ready bit
					op_state <= St4 ;
			      end
			St4 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_lun0_cache_reg1_page0 (state_count == 17) ;
		case (op_state)
			St1 : begin
				//Send 00h command. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St2 ;
			      end
			St2 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St3 ;
				if(col_addr > 0) // that is read due to every even re_n active toggle.
				begin
					if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_A)
						error_count <= error_count + 1;
				end
				col_addr <= col_addr + 1;
			      end
			St3 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St4 ;
			      end
			St4 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St5 ;
				//Here read due to every odd re_n active toggle.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_5)
					error_count <= error_count + 1;
			      end
			St5 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(col_addr < max_val )
				begin
					op_state <= St2 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St6 ;
					col_addr  <= 0;
				end
			      end
			St6 : begin
				//Last even read will be pending.Do it here.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St7 ;
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_A)
					error_count <= error_count + 1;
			     end
			St7 : begin
				//check errors.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(error_count > 0)
					$display("Read Error present in LUN0");
				error_count <= 0;
				op_state <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_lun0_cache_reg0_page1 (state_count == 18) ;
		case (op_state)
			St1 : begin
				//Need to select the cahce register 0 first to start reading . Send h06 command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_06);
				op_state <= St2 ;
			      end
			St2 : begin
				//send col address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//send col address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//send page address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_01);
				op_state <= St5 ;
			      end
			St5 : begin
				//send  address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//send  address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//send 'hE0 command to start reading
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_E0);
				op_state <= St8 ;
			      end
			St8 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St9 ;
				if(col_addr > 0) // that is read due to every even re_n active toggle.
				begin
					if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_A)
						error_count <= error_count + 1;
				end
				col_addr <= col_addr + 1;
			      end
			St9 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St11 ;
				//Here read due to every odd re_n active toggle.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_5)
					error_count <= error_count + 1;
			      end
			St11 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(col_addr < max_val )
				begin
					op_state <= St8 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St12 ;
					col_addr  <= 0;
				end
			      end
			St12 : begin
				//Last even read will be pending.Do it here.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St13 ;
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_A)
					error_count <= error_count + 1;
			     end
			St13 : begin
				//check errors.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(error_count > 0)
					$display("Read Error present in LUN0");
				error_count <= 0;
				op_state <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_finish_simulation (state_count == 19) ;
		$finish ;
	endrule
*/
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////                                           END OF TEST5                                               /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Test6 : Write into 4 pages in plane0 of lun0 by means of multi-plane program page cache.Then Erase 2 pages using multi-plane erase                                   ////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*	
	//Write into addr0(plane0) and adddr0(plane1) in LUN0. 
	rule rl_writing_into_lun0_plane0_page0 (state_count == 3) ;
		case (op_state)
			St1 : begin
				//Send 80h first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_80);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Start sending data by toggling we_n pin.
				//Disable we.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St8 ;
			      end
			St8 : begin
				//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_A);
				op_state <= St9 ;
				col_addr <= col_addr + 1;
			      end
			St9 : begin
				//Disable we again
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
	                       	//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_5);
				if(col_addr < max_val)
				begin
					op_state <= St7 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St11 ;
					col_addr <= 0 ;
				end
			      end
			St11 : begin
				//End command series by giving 11h. To queue program
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_11);
				op_state <= St12 ;
			      end
			St12 : begin
				//Disable all control signals 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			        op_state <= St1;// Observe rdy flags in waveform.
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_writing_into_lun1_plane1_page0 (state_count == 4) ;
		case (op_state)
			St1 : begin
				//Send 80h first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_80);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h18);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Start sending data by toggling we_n pin.
				//Disable we.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St8 ;
			      end
			St8 : begin
				//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_A);
				op_state <= St9 ;
				col_addr <= col_addr + 1;
			      end
			St9 : begin
				//Disable we again
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
	                       	//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_5);
				if(col_addr < max_val)
				begin
					op_state <= St7 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St11 ;
					col_addr <= 0 ;
				end
			      end
			St11 : begin
				//End command series by giving 15h. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h15);
				op_state <= St12 ;
			      end
			St12 : begin
				//Disable all control signals 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			        op_state <= St1;// Observe rdy flags in waveform.
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_status_first (state_count == 5) ;  
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 70h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_70);
			      end
			St2 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St3 ;
			      end
			St3 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[6] == busy_active_low) //7th bit is the Ready bit
					op_state <= St4 ;
			      end
			St4 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_writing_into_lun0_plane0_page1 (state_count == 6) ;
		case (op_state)
			St1 : begin
				//Send 80h first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_80);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_01);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Start sending data by toggling we_n pin.
				//Disable we.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St8 ;
			      end
			St8 : begin
				//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_5);
				op_state <= St9 ;
				col_addr <= col_addr + 1;
			      end
			St9 : begin
				//Disable we again
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
	                       	//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_A);
				if(col_addr < max_val)
				begin
					op_state <= St7 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St11 ;
					col_addr <= 0 ;
				end
			      end
			St11 : begin
				//End command series by giving 11h. To queue program
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_11);
				op_state <= St12 ;
			      end
			St12 : begin
				//Disable all control signals 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			        op_state <= St1;// Observe rdy flags in waveform.
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_writing_into_lun1_plane1_page1 (state_count == 7) ;
		case (op_state)
			St1 : begin
				//Send 80h first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_80);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h19);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Start sending data by toggling we_n pin.
				//Disable we.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St8 ;
			      end
			St8 : begin
				//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_5);
				op_state <= St9 ;
				col_addr <= col_addr + 1;
			      end
			St9 : begin
				//Disable we again
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
	                       	//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_A);
				if(col_addr < max_val)
				begin
					op_state <= St7 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St11 ;
					col_addr <= 0 ;
				end
			      end
			St11 : begin
				//End command series by giving 15h. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h15);
				op_state <= St12 ;
			      end
			St12 : begin
				//Disable all control signals 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			        op_state <= St1;// Observe rdy flags in waveform.
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_status_second (state_count == 8) ;  
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 70h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_70);
			      end
			St2 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St3 ;
			      end
			St3 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[5] == busy_active_low) //6th bit is the AReady bit
					op_state <= St4 ;
			      end
			St4 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule	
	
	rule rl_erase_page0_plane0 (state_count == 9) ;
		case (op_state)
			St1 : begin
				//Send 60h first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h60);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//End command series by giving D1h. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('hD1);
				op_state <= St7 ;
			      end
			St7 : begin
				//Disable all control signals 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			        op_state <= St1;// Observe rdy flags in waveform.
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_erase_page1_plane1 (state_count == 10) ;
		case (op_state)
			St1 : begin
				//Send 60h first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h60);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h18);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//End command series by giving D1h. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('hD0);
				op_state <= St7 ;
			      end
			St7 : begin
				//Disable all control signals 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			        op_state <= St1;// Observe rdy flags in waveform.
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_status_third (state_count == 11) ;  
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 70h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_70);
			      end
			St2 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St3 ;
			      end
			St3 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[6] == busy_active_low) //7th bit is the Ready bit
					op_state <= St4 ;
			      end
			St4 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_lun0_data_plane0_page0 (state_count == 12) ;
		case (op_state)
			St1 : begin
				//Send 00h command first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Send 32h command to queue plane0 for read. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_32);
				op_state <= St8 ;
			      end
			St8 : begin
				op_state <= St1 ;
				state_count <= state_count + 1;
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			      end
		endcase
	endrule
	
	rule rl_read_lun0_data_plane1_page0 (state_count == 13) ;
		case (op_state)
			St1 : begin
				//Send 00h command first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h18);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Send 30h command to start reading queued and present page. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_30);
				op_state <= St8 ;
			      end
			St8 : begin
				op_state <= St1 ;
				state_count <= state_count + 1;
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			      end
		endcase
	endrule
	
	rule rl_read_status_fourth (state_count == 14) ;  
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 70h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_70);
			      end
			St2 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St3 ;
			      end
			St3 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[6] == busy_active_low) //7th bit is the Ready bit
					op_state <= St4 ;
			      end
			St4 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_lun0_cache_reg1_page1 (state_count == 15) ;
		case (op_state)
			St1 : begin
				//Send 00h command. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St2 ;
			      end
			St2 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St3 ;
				if(col_addr > 0) // that is read due to every even re_n active toggle.
				begin
					if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != 'hFF)
						error_count <= error_count + 1;
				end
				col_addr <= col_addr + 1;
			      end
			St3 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St4 ;
			      end
			St4 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St5 ;
				//Here read due to every odd re_n active toggle.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != 'hFF)
					error_count <= error_count + 1;
			      end
			St5 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(col_addr < max_val )
				begin
					op_state <= St2 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St6 ;
					col_addr  <= 0;
				end
			      end
			St6 : begin
				//Last even read will be pending.Do it here.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St7 ;
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != 'hFF)
					error_count <= error_count + 1;
			     end
			St7 : begin
				//check errors.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(error_count > 0)
					$display("Read Error present in LUN0");
				error_count <= 0;
				op_state <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_lun0_cache_reg0_page0 (state_count == 16) ;
		case (op_state)
			St1 : begin
				//Need to select the cahce register 0 first to start reading . Send h06 command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_06);
				op_state <= St2 ;
			      end
			St2 : begin
				//send col address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//send col address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//send  address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St5 ;
			      end
			St5 : begin
				//send  address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//send  address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//send 'hE0 command to start reading
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_E0);
				op_state <= St8 ;
			      end
			St8 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St9 ;
				if(col_addr > 0) // that is read due to every even re_n active toggle.
				begin
					if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != 'hFF)
						error_count <= error_count + 1;
				end
				col_addr <= col_addr + 1;
			      end
			St9 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St11 ;
				//Here read due to every odd re_n active toggle.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != 'hFF)
					error_count <= error_count + 1;
			      end
			St11 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(col_addr < max_val )
				begin
					op_state <= St8 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St12 ;
					col_addr  <= 0;
				end
			      end
			St12 : begin
				//Last even read will be pending.Do it here.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St13 ;
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != 'hFF)
					error_count <= error_count + 1;
			     end
			St13 : begin
				//check errors.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(error_count > 0)
					$display("Read Error present in LUN0");
				error_count <= 0;
				op_state <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_lun0_data_plane0_page1 (state_count == 17) ;
		case (op_state)
			St1 : begin
				//Send 00h command first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_01);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Send 32h command to queue plane0 for read. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_32);
				op_state <= St8 ;
			      end
			St8 : begin
				op_state <= St1 ;
				state_count <= state_count + 1;
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			      end
		endcase
	endrule
	
	rule rl_read_lun0_data_plane1_page1 (state_count == 18) ;
		case (op_state)
			St1 : begin
				//Send 00h command first
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//Send col addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h19);
				op_state <= St5 ;
			      end
			St5 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//Send  addr. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//Send 30h command to start reading queued and present page. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_30);
				op_state <= St8 ;
			      end
			St8 : begin
				op_state <= St1 ;
				state_count <= state_count + 1;
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			      end
		endcase
	endrule
	
	rule rl_read_status_fifth (state_count == 19) ;  
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				//Send 70h command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_70);
			      end
			St2 : begin
				//Enable re_n and wait. data will arrive next cycle.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				op_state <= St3 ;
			      end
			St3 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low); //enable re_n to read the status.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m()[6] == busy_active_low) //7th bit is the Ready bit
					op_state <= St4 ;
			      end
			St4 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				op_state  <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_lun0_cache_reg1_page0 (state_count == 20) ;
		case (op_state)
			St1 : begin
				//Send 00h command. 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St2 ;
			      end
			St2 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St3 ;
				if(col_addr > 0) // that is read due to every even re_n active toggle.
				begin
					if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != 'hFF)
						error_count <= error_count + 1;
				end
				col_addr <= col_addr + 1;
			      end
			St3 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St4 ;
			      end
			St4 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St5 ;
				//Here read due to every odd re_n active toggle.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != 'hFF)
					error_count <= error_count + 1;
			      end
			St5 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(col_addr < max_val )
				begin
					op_state <= St2 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St6 ;
					col_addr  <= 0;
				end
			      end
			St6 : begin
				//Last even read will be pending.Do it here.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St7 ;
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != 'hFF)
					error_count <= error_count + 1;
			     end
			St7 : begin
				//check errors.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(error_count > 0)
					$display("Read Error present in LUN0");
				error_count <= 0;
				op_state <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_lun0_cache_reg0_page1 (state_count == 21) ;
		case (op_state)
			St1 : begin
				//Need to select the cahce register 0 first to start reading . Send h06 command
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_06);
				op_state <= St2 ;
			      end
			St2 : begin
				//send col address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//send col address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St4 ;
			      end
			St4 : begin
				//send  address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_01);
				op_state <= St5 ;
			      end
			St5 : begin
				//send  address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St6 ;
			      end
			St6 : begin
				//send  address
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St7 ;
			      end
			St7 : begin
				//send 'hE0 command to start reading
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_E0);
				op_state <= St8 ;
			      end
			St8 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St9 ;
				if(col_addr > 0) // that is read due to every even re_n active toggle.
				begin
					if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != 'hFF)
						error_count <= error_count + 1;
				end
				col_addr <= col_addr + 1;
			      end
			St9 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St11 ;
				//Here read due to every odd re_n active toggle.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != 'hFF)
					error_count <= error_count + 1;
			      end
			St11 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(col_addr < max_val )
				begin
					op_state <= St8 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St12 ;
					col_addr  <= 0;
				end
			      end
			St12 : begin
				//Last even read will be pending.Do it here.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St13 ;
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != 'hFF)
					error_count <= error_count + 1;
			     end
			St13 : begin
				//check errors.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(error_count > 0)
					$display("Read Error present in LUN0");
				error_count <= 0;
				op_state <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_finish_simulation (state_count == 22) ;
		$finish ;
	endrule
*/			
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////                                           END OF TEST6                                               /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Test7 : Checking read id,parameter,feature,unique id, set feature commands                                                                                           ////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*	
	//Write into addr0(plane0) and adddr0(plane1) in LUN0. 
	rule rl_read_id_00 (state_count == 3) ;
		case (op_state)
			St1 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h90);
				op_state <= St2 ;
			      end
			St2 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St4 ;
			      end
			St4 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St5 ;
			      end
			St5 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St6 ;
			      end
			St6 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St7 ;
			      end
			St7 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St8 ;
			      end
			St8 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St9 ;
			      end
			St9 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St11 ;
			      end
			St11 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St12 ;
			      end
			St12 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St1 ;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_id_20 (state_count == 4) ;
		case (op_state)
			St1 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h90);
				op_state <= St2 ;
			      end
			St2 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('h20);
				op_state <= St3 ;
			      end
			St3 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St4 ;
			      end
			St4 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St5 ;
			      end
			St5 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St6 ;
			      end
			St6 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St7 ;
			      end
			St7 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St8 ;
			      end
			St8 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St9 ;
			      end
			St9 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St11 ;
			      end
			St11 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St12 ;
			      end
			St12 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St1 ;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_paramater_page (state_count == 5) ;
		case (op_state)
			St1 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('hEC);
				op_state <= St2 ;
			      end
			St2 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St4 ;
				col_addr <= col_addr + 1;
			      end
			St4 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St5 ;
			      end
			St5 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St6 ;
			      end
			St6 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(col_addr < 'hFF )
				begin
					op_state <= St3 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St7 ;
					col_addr  <= 0;
				end
			      end
			St7 : begin
				//Last even read will be pending.Do it here.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St8 ;
			     end
			St8 : begin
				//check errors.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_read_uid (state_count == 6) ;
		case (op_state)
			St1 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('hED);
				op_state <= St2 ;
			      end
			St2 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_00);
				op_state <= St3 ;
			      end
			St3 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St4 ;
				col_addr <= col_addr + 1;
			      end
			St4 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St5 ;
			      end
			St5 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St6 ;
			      end
			St6 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(col_addr < 'h1F )
				begin
					op_state <= St3 ;
					col_addr <= col_addr + 1;
				end
				else
				begin
					op_state <= St7 ;
					col_addr  <= 0;
				end
			      end
			St7 : begin
				//Last even read will be pending.Do it here.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St8 ;
			     end
			St8 : begin
				//check errors.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_set_features (state_count == 7) ;
		case (op_state)
			St1 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('hEF);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send addr
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_80);
				op_state <= St3 ;
			      end
			St3 : begin
				//Start sending data by toggling we_n pin.
				//Disable we.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St4 ;
			      end
			St4 : begin
				//Enable we and send data 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_5);
				op_state <= St5 ;
			      end
			St5 : begin
				//Disable we again
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St6 ;
			      end
			St6 : begin
	                       	//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_A);
				op_state <= St7 ;
			      end
			St7 : begin
				//Disable we again
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St8 ;
			      end
			St8 : begin
	                       	//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_5);
				op_state <= St9 ;
			      end
			St9 : begin
				//Disable we again
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
	                       	//Enable we and send data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(data_A);
				op_state <= St11 ;
			      end
			St11 : begin
				//Disable all control signals 
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
			        op_state <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_get_features_1 (state_count == 8) ;
		case (op_state)
			St1 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('hEE);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send addr
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_80);
				op_state <= St3 ;
			      end
			St3 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St4 ;
			      end
			St4 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St5 ;
			      end
			St5 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St6 ;
				//Here read due to every odd re_n active toggle.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_5)
					error_count <= error_count + 1;
			      end
			St6 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St7 ;
			      end
			St7 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St8 ;
				//Here read due to every odd re_n active toggle.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_A)
					error_count <= error_count + 1;
			      end
			St8 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St9 ;
			      end
			St9 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
				//Here read due to every odd re_n active toggle.
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_5)
					error_count <= error_count + 1;
			      end
			St10 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St11 ;
			      end
			St11 : begin
				//Last even read will be pending.Do it here.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St12 ;
				if(nandFlashTarget.onfi_target_interface._data_to_nfc_m() != data_A)
					error_count <= error_count + 1;
			     end
			St12 : begin
				//check errors.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				if(error_count > 0)
					$display("Read Error present in FEATURE MEMORY");
				error_count <= 0;
				op_state <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_get_features_2 (state_count == 9) ;
		case (op_state)
			St1 : begin
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m('hEE);
				op_state <= St2 ;
			      end
			St2 : begin
				//Send addr
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(enable_active_high);
				nandFlashTarget.onfi_target_interface._data_from_nfc_m(cmd_10);
				op_state <= St3 ;
			      end
			St3 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St4 ;
			      end
			St4 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St5 ;
			      end
			St5 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St6 ;
			      end
			St6 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St7 ;
			      end
			St7 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St8 ;
			      end
			St8 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St9 ;
			      end
			St9 : begin
				//activate re_n. Need to toggle read_enable to read data.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St10 ;
			      end
			St10 : begin
				//Disable re_n.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St11 ;
			      end
			St11 : begin
				//Last even read will be pending.Do it here.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St12 ;
			     end
			St12 : begin
				//check errors.
				nandFlashTarget.onfi_target_interface._onfi_ce_n_m(enable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_wp_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_we_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_re_n_m(disable_active_low);
				nandFlashTarget.onfi_target_interface._onfi_cle_m(disable_active_high);
				nandFlashTarget.onfi_target_interface._onfi_ale_m(disable_active_high);
				op_state <= St1;
				state_count <= state_count + 1;
			      end
		endcase
	endrule
	
	rule rl_finish_simulation (state_count == 10) ;
		$finish ;
	endrule
*/	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////                                           END OF TEST7                                               /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 

endmodule
