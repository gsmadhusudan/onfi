package InterfaceNandFlashController ;

import Vector::*;
`include "global_parameters_Flash.bsv"
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//		NVM - NAND FLASH CONTROLLER INTERFACE
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
interface NandFlashInterface   ;

	method Action _request_data(UInt#(64) _address, UInt#(11) _length);
	method Action _request_erase(UInt#(64) _address); //To erase a block of data
	method Bit#(`WDC) _get_data_();
	method Action _write(UInt#(64) _address, Bit#(`WDC) _data, UInt#(11) _length);
	method Action _enable(bit _nand_ce_l);
	method Action _query_bad_block(UInt#(64) _address);
	method bit interrupt_();
	method bit busy_();
	method bit write_success_();
	method bit write_fail_();
	method bit erase_success_();
	method bit erase_fail_();

endinterface


// Methods for NVM-NFC interface definitions
function NandFlashInterface fn_nvm_nfc_interface (Wire#(Bit#(64)) wr_address_from_nvm,Wire#(Bit#(`WDC)) wr_data_from_nvm,Wire#(Bit#(11)) wr_w_length,Wire#(Bit#(11)) wr_r_length, Reg#(Bit#(`WDC)) rg_data_to_nvm,Wire#(Bit#(1)) wr_nand_ce_n,Wire#(Bit#(1)) wr_nand_we_n, Wire#(Bit#(1)) wr_nand_re_n,Reg#(Bit#(1)) rg_interrupt,Reg#(Bit#(1)) rg_ready_busy_n, Reg#(Bit#(1)) wr_nand_erase, Reg#(Bit#(1)) rg_write_success, Reg#(Bit#(1)) rg_write_fail, Reg#(Bit#(1)) rg_erase_success, Reg#(Bit#(1)) rg_erase_fail, Wire#(Bit#(1)) wr_nand_bbm_n) ;
	
	return (interface NandFlashInterface ;
                      
		      method Action _request_data(_address,_length) ;
              	              wr_nand_re_n	  <= 1'b0     ;
              	              wr_address_from_nvm <= pack(_address) ;
			      wr_r_length         <= pack(_length) ;
              	      endmethod: _request_data
		      
		      method Action _request_erase(_address) ;
              	              wr_nand_erase	  <= 1'b1     ;
              	              wr_address_from_nvm <= pack(_address) ;
              	      endmethod: _request_erase

              	      method Bit#(`WDC) _get_data_() ;
              	              return rg_data_to_nvm ;
              	      endmethod: _get_data_

              	      method Action _write(_address, _data, _length) ;
              	              wr_nand_we_n	  <= 1'b0     ;
              	              wr_address_from_nvm <= pack(_address) ;
              	              wr_data_from_nvm    <= _data    ;       
			      wr_w_length         <= pack(_length) ;
              	      endmethod: _write

              	      method Action _enable(bit _nand_ce_l);
              	              wr_nand_ce_n        <= _nand_ce_l;
              	      endmethod: _enable

              	      method bit interrupt_() ;
              	              return rg_interrupt;           //Interrupt when read data is ready 
              	      endmethod: interrupt_

              	      method bit busy_() ;              	              
              	              return (~rg_ready_busy_n);     //Return busy state of the NAND Controller
              	      endmethod: busy_
		      
		      method bit write_success_() ;              	              
              	              return (rg_write_success);     
              	      endmethod: write_success_
		      
		      method bit write_fail_() ;              	              
              	              return (rg_write_fail);     
              	      endmethod: write_fail_
		      
		      method bit erase_success_() ;              	              
              	              return (rg_erase_success);     
              	      endmethod: erase_success_
		      
		      method bit erase_fail_() ;              	              
              	              return (rg_erase_fail);     
              	      endmethod: erase_fail_
		      
		      method Action _query_bad_block(_address) ;
              	              wr_nand_bbm_n	  <= 1'b0     ;
              	              wr_address_from_nvm <= pack(_address) ;
              	      endmethod: _query_bad_block
		      
		endinterface );
endfunction


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//		NAND FLASH CONTROLLER - ONFi INTERFACE
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
interface ONFiInterface   ;
 
	method bit onfi_ce0_n_ () ;
	method bit onfi_ce1_n_ () ;
	method bit onfi_we_n_ () ;
	method bit onfi_re_n_ () ;
	method bit onfi_wp_n_ () ;
	method bit onfi_cle_ () ;
	method bit onfi_ale_ () ;
	method Action _ready_busy0_n_m ( bit _ready_busy0_l ) ;
	method Action _ready_busy1_n_m ( bit _ready_busy1_l ) ;
	interface Inout#(Bit#(8)) dataio0 ;
	interface Inout#(Bit#(8)) dataio1 ;

endinterface

//Methods for NFC-ONFI interface definitions
function ONFiInterface fn_nfc_onfi_interface (Reg#(bit) rg_onfi_ce0_n,Reg#(bit) rg_onfi_ce1_n,Reg#(bit) rg_onfi_cle,Reg#(bit) rg_onfi_ale,Reg#(bit) rg_onfi_we_n,Reg#(bit) rg_onfi_re_n,Reg#(bit) rg_onfi_wp_n,Wire#(bit) wr_ready_busy0_n,Wire#(bit) wr_ready_busy1_n,Inout#(Bit#(8)) data0,Inout#(Bit#(8)) data1) ;
	
	return (interface ONFiInterface ;
		       
		       interface dataio0 = data0 ;
		       interface dataio1 = data1 ;
		       
		       method bit onfi_ce0_n_ () ;
	                       return rg_onfi_ce0_n ;
	               endmethod: onfi_ce0_n_
		      
		       method bit onfi_ce1_n_ () ;
	                       return rg_onfi_ce1_n ;
	               endmethod: onfi_ce1_n_
		       	
	               method bit onfi_we_n_ () ;
	                       return rg_onfi_we_n ;
	               endmethod: onfi_we_n_
	
	               method bit onfi_re_n_ () ;
	                       return rg_onfi_re_n ;
	               endmethod: onfi_re_n_
	
	               method bit onfi_wp_n_ () ;
	                       return rg_onfi_wp_n ;
	               endmethod: onfi_wp_n_
	
	               method bit onfi_cle_ () ;
	                       return rg_onfi_cle ;
	               endmethod: onfi_cle_
	
	               method bit onfi_ale_ () ;
	                       return rg_onfi_ale ;
	               endmethod: onfi_ale_
	
	               method Action _ready_busy0_n_m ( _ready_busy_n ) ;
	                       wr_ready_busy0_n <= _ready_busy_n ;
	               endmethod: _ready_busy0_n_m
		       
		       method Action _ready_busy1_n_m ( _ready_busy_n ) ;
	                       wr_ready_busy1_n <= _ready_busy_n ;
	               endmethod: _ready_busy1_n_m
		
		endinterface );
endfunction
endpackage
